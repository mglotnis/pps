
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});





var events = [

    {'Date': new Date(2018, 5, 18), 'Title 1': 'Doctor appointment at 3:25pm.'},

    {'Date': new Date(2018, 5, 16), 'Title 2': 'New Garfield movie comes out!', 'Link': 'https://garfield.com'},

    {'Date': new Date(2018, 5, 20), 'Title 3': '25 year anniversary', 'Link': 'https://www.google.com.au/#q=anniversary+gifts'},

];


var settings = {

    Color: '',

    LinkColor: '',

    NavShow: true,

    NavVertical: false,

    NavLocation: '',

    DateTimeShow: true,

    DateTimeFormat: 'mmm, yyyy',

    DatetimeLocation: '',

    EventClick: '',

    EventTargetWholeDay: false,

    DisabledDays: [],

    ModelChange: model

};


var element = document.getElementById('caleandar');

caleandar(element, events, settings);




