<div class="navigation">

	<div class="logobox">
		<a href="/"><img src="{{asset('/images/PPS.jpg' ) }}"></a>
	</div>
		
	<nav>
		<ul>
			<li>
				<a href="/about">Apie mus</a>
			</li>

			<li>
				<a href="/gallery">Galerija</a>
			</li>

			<li>
				<a href="/members">Nariai</a>
			</li>

			<li>
				<a href="/news">Naujienos</a>
			</li>
			
			<li>
				<a href="/contacts">Kontaktai</a>
			</li>
		</ul>
	</nav>
</div>